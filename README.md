# Selenium-Auto-Test

This is for testing websites with selenium.


# What do I need ?

1. Java(JDK)
2. IntelliJ (or what ever you want)
3. Web Driver -> Firefox: https://github.com/mozilla/geckodriver/releases (This code is written for firefox)

# How to configure
<br>
Install via Maven

# How to use it ?
<br>
An example is in src/test.

1. Go to src/main/ressources and edit the config.json 
    
    What do we have here ?<br>
    
    ```json
       "fields": [     # fields are the inputs for email/username and the password (of course you use an test account)
            {
              "name":"E-Mail",          # this is just for the console messages and that you know what field that is
              "fieldName":"username",   # field name is the name attribute of the email/user element
              "value":"Penguin"         # this the value you want to enter
            },
            {
              "name":"Password",
              "fieldName":"password",   # you can also use fieldClass
              "value":"Password1"
            }
        ],
        "url":"website_you_want_to_test",           # this is the website you start your tests
        "login_path":"/#/login",                    # this is the path after login (just for checking that your login was successful)
        "login_btn_name":"button_login_submit"      # login button (name)
    ```
2. Go to the json for your test e.g. : src/main/ressources/invalidLogin.java<br>
    and edit it with your data
