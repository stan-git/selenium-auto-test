import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.junit.Assert.*;

/**
 *
 * @author Janik
 * @version 1.0
 *
 */

class Toolbox {

    static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";

    static final String JSON_PATH = System.getProperty("user.dir") + "/src/main/resources/";
    private JsonObject configObj;
    private FirefoxDriver driver = null;
    private WebDriverWait wait;
    
    Toolbox(){
        this.configObj = openConfig("config.json");
    }

    /**
     * Open the test config
     * @exception FileNotFoundException if file is not at the right place
     * @return the config.json(JsonObject) if no error, else null
     */
    JsonObject openConfig(String configName) {
        JsonParser parser = new JsonParser();
        try {
            Object obj = parser.parse(new FileReader(JSON_PATH + configName));

            return (JsonObject) obj;

        } catch (FileNotFoundException e) {
            System.out.println("["+ANSI_RED+"!"+ANSI_RESET+"] Json file not found  in \"" + JSON_PATH + "config.json\"");
        }
        return null;
    }

    /**
     * Creates the Firefoxdriver
     * @param headless (boolean) for the option to run the browser headless
     * @return driver(FirefoxDriver)
     */
    FirefoxDriver createDriver(boolean headless) {
        if (headless) {
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            firefoxBinary.addCommandLineOptions("--headless");
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setBinary(firefoxBinary);
            this.driver = new FirefoxDriver(firefoxOptions);
        } else {

            this.driver = new FirefoxDriver();
            this.driver.manage().window().setPosition(new Point(1300,800));
            this.driver.manage().window().maximize();
        }

        this.wait = new WebDriverWait(this.driver, 15);
        return this.driver;
    }

    /**
     * This will open the given homepage where you want to test
     * The url is saved in the config.json
     * @throws Error if there is no driver
     */
    void openTestSite() {
        assertNotNull("["+ANSI_RED+"!"+ANSI_RESET+"] Please first create a driver ", this.driver);

        String url = this.configObj.get("url").getAsString();
        try {
            this.driver.get(url);
        } catch (org.openqa.selenium.InvalidArgumentException e) {
            System.out.println("["+ANSI_RED+"!"+ANSI_RESET+"] Check you url  in \"" + JSON_PATH + "config.json\"");
            System.exit(1);
        }

        this.wait.until(ExpectedConditions.urlToBe(url));
        String u = this.driver.getCurrentUrl();
        assertTrue("["+ANSI_RED+"!"+ANSI_RESET+"] url doesn't changed ",u.matches(url));

    }

    /**
     * This will login to the testsite
     * @throws Error if the url doesn't change
     */
    void login() {
        System.out.println("\n++++++++++++ "+
                ANSI_PURPLE+"LOGIN"+
                ANSI_RESET+ " ++++++++++++\n");

        assertNotNull("["+ANSI_RED+"!"+ANSI_RESET+"] Please first create a driver ", this.driver);

        //open the test site enter pw and username and click login
        openTestSite();

        fillFields(this.configObj.getAsJsonArray("fields"));

        System.out.println("["+ANSI_YELLOW+"*"+ANSI_RESET+"] Click [Login]\n");

        this.driver.findElementByName(this.configObj.get("login_btn_name")
                .toString().replace("\"","")).click();

        //wait for the login and check url
        this.wait.until(ExpectedConditions.urlContains(this.configObj.get("login_path").getAsString()));

        assertTrue("["+ANSI_RED+"!"+ANSI_RESET+"] login failed", this.driver
                        .getCurrentUrl()
                        .contains(this.configObj.get("login_path").getAsString()));
    }

    /**
     * This is to fill out a form (the text fields)
     * It will automatic enter every given input (in test json)
     * @param fields (JsonArray) is a array with data about input fields
     */
    void fillFields(JsonArray fields) {

        //loop through the list of inputs and get the name
        for (JsonElement fieldEl : fields) {
            JsonObject obj = fieldEl.getAsJsonObject();

            WebElement field;
            String name = obj.get("name").getAsString();
            String value = obj.get("value").getAsString();

            if(obj.has("fieldClass")){
                String fieldClass = obj.get("fieldClass").getAsString();
                field = this.driver.findElementByClassName(fieldClass);
            }else   {
                String fieldName = obj.get("fieldName").getAsString();

                sleep(500);
                field = (obj.has("useParent") ?
                        this.driver.findElementByCssSelector("."+obj
                                .get("useParent")
                                .getAsString()+" [name="+fieldName+"]") :
                        this.driver.findElementByName(fieldName));
            }

            this.wait.until(ExpectedConditions.visibilityOf(field));
            System.out.println("["+ANSI_YELLOW+"*"+ANSI_RESET+"] Enter ["+name+"]\tValue: "+value);
            sleep(1000);
            //reset the field
            field.clear();
            field.sendKeys(value);
        }
    }

    /**
     * This is for dropdown lists
     * It will automatically choose the given option (in test json)
     * @param dropdownArray is a array with data about dropdowns
     */
    void dropdown(JsonArray dropdownArray) {
        for (JsonElement dropdownEl : dropdownArray) {
            JsonObject obj = (JsonObject) dropdownEl;
            WebElement dropdown = this.driver.findElementByName(obj.get("dropdownName").getAsString());

            this.wait.until(ExpectedConditions.visibilityOf(dropdown));
            Select select = new Select(dropdown);
            select.selectByVisibleText(obj.get("value").getAsString());

            System.out.print("["+ANSI_YELLOW+"*"+ANSI_RESET+"] Dropdown ["+obj.get("name").getAsString()+"]\n");
        }
    }

    /**
     * This is for the inputs type = checkbox
     * If it'll checked or not is saved in the test json
     * @param checkBoxArray (JsonArray) is a array with data about checkboxes
     */
    void check(JsonArray checkBoxArray) {
        for (JsonElement checkBoxObj : checkBoxArray) {
            JsonObject obj = (JsonObject) checkBoxObj;

            WebElement checkB = obj.has("useParent") && obj.has("boxClass") ?
                    this.driver.findElementByCssSelector("."+obj.get("useParent").getAsString()+" [class="+obj.get("boxClass").getAsString()+"]") :
                    this.driver.findElementByClassName(obj.get("boxClass").getAsString());

            this.wait.until(ExpectedConditions.visibilityOf(checkB));
            System.out.print("["+ANSI_YELLOW+"*"+ANSI_RESET+"] Click CheckBox ["+obj.get("name").getAsString()+"]\n");

            this.driver.executeScript("arguments[0].scrollIntoView(true);",checkB);
            this.sleep(500);

            checkB.click();
        }
    }

    /**
     * This is for asserts
     * Here you can check if a test was successful
     * @param assertsArray (JsonArray) array with data for the assert
     */
    public void asserts(JsonArray assertsArray) {
        WebElement element = null;
        for (JsonElement assertObj: assertsArray) {
            JsonObject obj = (JsonObject) assertObj;

            sleep(1000);
            if (obj.has("elementName"))
                element = this.driver
                        .findElementByName(obj
                                .get("elementName")
                                .getAsString());

            if (obj.has("elementXPath"))
                element = this.driver
                        .findElementByXPath(obj
                                .get("elementXPath")
                                .getAsString());

            if (obj.has("elementCSS"))
                element = this.driver
                        .findElementByCssSelector(obj
                                .get("elementCSS")
                                .getAsString());


            //chill and then get the element
            this.wait.until(ExpectedConditions.textToBePresentInElement(element,obj.get("value")
                    .getAsString()));

            assertEquals(String.format("[%s!%s] Element [%s] doesn't exist or not the right Text",
                    ANSI_RED, ANSI_RESET, obj.get("name")),
                    obj.get("value")
                            .getAsString(), element != null ? element.getText() : null);

            sleep(500);

        }
    }

    /**
     * Just a sleep function
     * @param ms (int) milliseconds
     */
    void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
