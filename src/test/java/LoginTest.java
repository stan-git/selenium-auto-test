import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class LoginTest {

    private Toolbox tb = new Toolbox();
    private FirefoxDriver driver = tb.createDriver(true);


    @Test
    public void invalidLogin() {
        System.out.println("\n++++++++++++ "+
                Toolbox.ANSI_BLUE+"INVALID LOGIN TEST"+
                Toolbox.ANSI_RESET+ " ++++++++++++\n");

        JsonParser parser = new JsonParser();
        tb.openTestSite();

        JsonObject jsonObject = tb.openConfig("invalidLogin.json");

        //keyset.size to get the number of tests and loop through the tests
        for (int j = 1; j <= jsonObject.keySet().size(); j++) {

            System.out.println("\n*********** "+Toolbox.ANSI_BLUE+"Test" + j + Toolbox.ANSI_RESET+ " ***********\n");

            JsonObject test = jsonObject.getAsJsonObject("Test"+j);

            tb.fillFields(test.getAsJsonArray("fields"));
            System.out.println("[*] Click ["+test.getAsJsonObject("button").get("name").getAsString()+"]");
            driver.findElementByName(test.getAsJsonObject("button").get("btnName").getAsString()).click();

            tb.asserts(test.getAsJsonArray("asserts"));
        }
    }

    @After
    public void closeDriver() {   driver.quit(); }
}
